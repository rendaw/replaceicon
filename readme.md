# replaceicon!

A tool to replace window icons.  It is pretty light weight.

You might want this if you run multiple browser instances.

You might want to run it like:

```
replaceicon firefox_teal.png run -- firefox -new-instance -P myprofile
```

and get a Firefox with a cool teal fire-fox icon.

For your convenience some Firefox icons with varied colors are in [icons/](icons/).

## Install

Run
```
git clone https://gitlab.com/rendaw/replaceicon.git
cd replaceicon
cargo install --path .
```

Let me know if it would help to have on [crates.io](crates.io).
