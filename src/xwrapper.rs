use crate::error::Error;
use core::ptr;
use futures::stream::FilterMap;
use futures::{try_ready, Async, Poll, Stream};
use log::error;
use mio::unix::EventedFd;
use mio::Evented;
use mio::{PollOpt, Ready, Token};
use std::ffi::CString;
use std::mem;
use std::os::raw::{c_int, c_long, c_uchar, c_uint, c_ulong};
use std::os::unix::io::RawFd;
use std::ptr::{null, null_mut};
use std::sync::{Arc, Mutex};
use tokio::io;
use tokio::reactor::PollEvented2;
use x11::xlib;
use x11::xlib::{AnyPropertyType, XEvent};

pub struct X11 {
    display: *mut xlib::_XDisplay,
}

unsafe impl Send for X11 {}

impl X11 {
    pub fn new() -> Result<Arc<Mutex<X11>>, Error> {
        unsafe {
            let display = xlib::XOpenDisplay(null());
            if display == null_mut() {
                return err!("Can't open display");
            }
            unsafe extern "C" fn handle_x_error(
                _display: *mut xlib::Display,
                event: *mut xlib::XErrorEvent,
            ) -> c_int {
                error!(
                    target: "xlib",
                    "XLib call failed: {}\nMajor opcode: {}\nMinor opcode: {}\nResource id: {:x?}\nRequest serial number: {}",
                    (*event).error_code,
                    (*event).request_code,
                    (*event).minor_code,
                    (*event).resourceid,
                    (*event).serial,
                );
                0
            }
            xlib::XSetErrorHandler(Some(handle_x_error));
            Ok(Arc::new(Mutex::new(X11 { display: display })))
        }
    }

    pub fn atom(&self, name: &str) -> xlib::Atom {
        let name_str = Box::new(CString::new(name).unwrap());
        unsafe { xlib::XInternAtom(self.display, name_str.as_ptr(), xlib::False) }
    }

    pub fn query_tree(&self, target: xlib::Window) -> Result<Vec<xlib::Window>, Error> {
        let mut root: xlib::Window = 0;
        let mut parent: xlib::Window = 0;
        let mut children: *mut xlib::Window = ptr::null_mut();
        let mut children_count: c_uint = 0;
        unsafe {
            if xlib::XQueryTree(
                self.display,
                target,
                &mut root,
                &mut parent,
                &mut children,
                &mut children_count,
            ) == 0
            {
                return err!("XQueryTree failed");
            }
            let mut out = Vec::new();
            for _ in 0..children_count {
                out.push(*children);
                children = children.offset(1);
            }
            Ok(out)
        }
    }

    pub fn root_window(&self) -> xlib::Window {
        unsafe { xlib::XDefaultRootWindow(self.display) }
    }

    pub fn listen(&self, window: xlib::Window, mask: c_long) {
        unsafe {
            xlib::XSelectInput(self.display, window, mask);
        }
    }

    pub fn set_prop(
        &self,
        window: xlib::Window,
        atom: xlib::Atom,
        atom_type: xlib::Atom,
        format: c_int,
        data: Vec<u8>,
    ) -> Result<(), Error> {
        unsafe {
            if xlib::XChangeProperty(
                self.display,
                window,
                atom,
                atom_type,
                format,
                xlib::PropModeReplace,
                data.as_ptr(),
                (data.len() / (format as usize / 8)) as c_int,
            ) == 0
            {
                return err!("XChangeProperty failed");
            }
        }
        Ok(())
    }

    /*
    pub fn get_prop_string(
        &self,
        window: xlib::Window,
        atom: xlib::Atom,
    ) -> Result<Option<String>, Error> {
        match self.get_prop(window, atom)? {
            Some(b) => Ok(Some(String::from_utf8(b).map_err(Error::from)?)),
            None => Ok(None),
        }
    }
    */

    pub fn get_prop(
        &self,
        window: xlib::Window,
        atom: xlib::Atom,
    ) -> Result<Option<Vec<u8>>, Error> {
        const PROPERTY_BUFFER_SIZE: c_long = 1024; // 4k of RAM ought to be enough for anyone!
        let mut data: Vec<u8> = Vec::new();
        let mut offset = 0;

        let mut done = false;
        let mut actual_type = 0;
        let mut actual_format = 0;
        let mut quantity_returned = 0;
        let mut bytes_after = 0;
        let mut buf: *mut c_uchar = ptr::null_mut();
        while !done {
            let result = unsafe {
                xlib::XGetWindowProperty(
                    self.display,
                    window,
                    atom,
                    offset,
                    PROPERTY_BUFFER_SIZE,
                    xlib::False,
                    AnyPropertyType as u64, /* ok because 0 */
                    &mut actual_type,
                    &mut actual_format,
                    &mut quantity_returned,
                    &mut bytes_after,
                    &mut buf,
                )
            };
            if result != 0 {
                return err!("Failed to get X11 window property");
            }
            if buf.is_null() {
                return Ok(None);
            }
            offset += PROPERTY_BUFFER_SIZE;
            unsafe {
                let new_data = std::slice::from_raw_parts(
                    buf as *mut u8,
                    (quantity_returned * actual_format as c_ulong / 8) as usize,
                );
                data.extend_from_slice(&new_data);
                xlib::XFree(buf as _);
            }
            done = bytes_after == 0;
        }
        Ok(Some(data))
    }
}

struct FdAdapter(RawFd);
impl Evented for FdAdapter {
    fn register(
        &self,
        poll: &mio::Poll,
        token: Token,
        interest: Ready,
        opts: PollOpt,
    ) -> io::Result<()> {
        EventedFd(&self.0).register(poll, token, interest, opts)
    }

    fn reregister(
        &self,
        poll: &mio::Poll,
        token: Token,
        interest: Ready,
        opts: PollOpt,
    ) -> io::Result<()> {
        EventedFd(&self.0).reregister(poll, token, interest, opts)
    }

    fn deregister(&self, poll: &mio::Poll) -> io::Result<()> {
        EventedFd(&self.0).deregister(poll)
    }
}

pub trait StreamX11 {
    fn stream(&self) -> FilterMap<X11Stream, fn(Option<XEvent>) -> Option<XEvent>>;
}

impl StreamX11 for Arc<Mutex<X11>> {
    fn stream(&self) -> FilterMap<X11Stream, fn(Option<XEvent>) -> Option<XEvent>> {
        // TODO make sure only one stream exists
        let x11 = self.lock().unwrap();
        let xfd = unsafe { xlib::XConnectionNumber(x11.display) };
        X11Stream {
            x: self.clone(),
            io: PollEvented2::new(FdAdapter(xfd)),
        }
        .filter_map(|v| v)
    }
}

pub struct X11Stream {
    x: Arc<Mutex<X11>>,
    io: PollEvented2<FdAdapter>,
}

impl Stream for X11Stream {
    type Item = Option<XEvent>;
    type Error = Error;

    fn poll(&mut self) -> Poll<Option<Self::Item>, Self::Error> {
        let x = self.x.lock().unwrap();
        let check = || -> Result<Option<XEvent>, Error> {
            unsafe {
                if xlib::XPending(x.display) > 0 {
                    let mut xev: XEvent = mem::uninitialized();
                    xlib::XNextEvent(x.display, &mut xev);
                    Ok(Some(xev))
                } else {
                    self.io
                        .clear_read_ready(mio::Ready::readable())
                        .map_err(Error::from)?;
                    Ok(None)
                }
            }
        };
        if let Some(xev) = check()? {
            return Ok(Async::Ready(Some(Some(xev))));
        }
        try_ready!(self.io.poll_read_ready(mio::Ready::readable()));
        if let Some(xev) = check()? {
            return Ok(Async::Ready(Some(Some(xev))));
        }
        Ok(Async::Ready(Some(None)))
    }
}
