use std::fmt::{Display, Formatter};

pub struct Error(pub Box<String>);

impl<T: std::error::Error> From<T> for Error {
    fn from(e: T) -> Self {
        Error(Box::new(e.to_string()))
    }
}

pub trait OptErrorEx<T> {
    fn as_err(self) -> Result<T, Error>;
}

impl<T> OptErrorEx<T> for Option<T> {
    fn as_err(self) -> Result<T, Error> {
        match self {
            Some(v) => Ok(v),
            None => Err(Error(Box::new("None!".into()))),
        }
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        f.write_str(self.0.as_str())
    }
}

#[macro_export]
macro_rules! err {
    ($w:expr) => {
        Err(Error(Box::new($w.to_string())))
    };
}

#[macro_export]
macro_rules! errf {
    ($($w:expr),*) => {
        Err(Error(Box::new(format!($($w),*).to_string())))
    }
}
