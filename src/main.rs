#[macro_use]
mod error;
mod xwrapper;

use crate::error::{Error, OptErrorEx};
use crate::xwrapper::{StreamX11, X11};
use byteorder::{LittleEndian, ReadBytesExt};
use chrono::offset::Local;
use clap::{App, AppSettings, Arg, SubCommand};
use futures::stream::Stream;
use futures::Future;
use lazy_static::lazy_static;
use log::{debug, error, info, trace};
use png::ColorType;
use regex::Regex;
use simplelog::{Config, Level, LevelFilter, TermLogger, TerminalMode};
use std::fs::File;
use std::io::{BufReader, Cursor, Read, Write};
use std::mem::size_of;
use std::ops::Add;
use std::os::raw::c_ulong;
use std::path::Path;
use std::process::Command;
use std::sync::{Arc, Mutex};
use std::time::{Duration, Instant};
use tokio::runtime;
use tokio_process::CommandExt;
use tokio_timer::clock::Clock;
use tokio_timer::Delay;
use ttl_cache::TtlCache;
use x11::xlib;
use x11::xlib::{NoEventMask, PropertyChangeMask, SubstructureNotifyMask, XEvent};

type Pid = u32;

fn attach<'a>(
    pid: Pid,
    icon: &'a str,
    tree: bool,
) -> Result<Box<Future<Item = (), Error = ()> + Send>, Error> {
    let x = X11::new()?;
    let atom_pid;
    let atom_icon;
    {
        let x = x.lock().unwrap();
        atom_pid = x.atom("_NET_WM_PID");
        atom_icon = x.atom("_NET_WM_ICON");
        x.listen(x.root_window(), SubstructureNotifyMask);
    }

    #[derive(Clone)]
    struct SetIcon {
        icon: String,
        atom_icon: xlib::Atom,
    }
    impl SetIcon {
        fn apply(&self, x: &X11, w: xlib::Window) -> Result<(), Error> {
            let mut source_bytes;
            let source_info;
            {
                let decoder = png::Decoder::new(File::open(&self.icon[..]).unwrap());
                let (source_info_, mut reader) = decoder.read_info().unwrap();
                source_info = source_info_;
                source_bytes = vec![0; source_info.buffer_size()];
                reader.next_frame(&mut source_bytes).unwrap();
            }
            if source_info.color_type != ColorType::RGBA {
                return errf!(
                    "Invalid color depth {:?}; must be RGBA",
                    source_info.color_type
                );
            }
            const DATA_SIZE: usize = size_of::<c_ulong>();
            let mut data: Vec<u8> = Vec::with_capacity(
                (source_info.width as usize * source_info.height as usize + 2) * DATA_SIZE,
            );
            data.extend_from_slice(&(source_info.width as c_ulong).to_le_bytes());
            data.extend_from_slice(&(source_info.height as c_ulong).to_le_bytes());
            for y in 0..source_info.height as usize {
                for x in 0..source_info.width as usize {
                    let mut temp: [u8; DATA_SIZE] = [0; DATA_SIZE]; // padded to c_ulong
                    let at =
                        (y * source_info.width as usize + x) * source_info.color_type.samples();
                    temp[0] = source_bytes[at + 2]; // B
                    temp[1] = source_bytes[at + 1]; // G
                    temp[2] = source_bytes[at + 0]; // R
                    temp[3] = source_bytes[at + 3]; // A
                    data.extend_from_slice(&temp);
                }
            }
            x.set_prop(w, self.atom_icon, xlib::XA_CARDINAL, 32, data)?;
            Ok(())
        }
    }
    let set_icon = SetIcon {
        icon: icon.to_string(),
        atom_icon: atom_icon,
    };

    struct GetPid {
        atom_pid: xlib::Atom,
    }
    impl GetPid {
        fn apply(&self, x: &X11, w: xlib::Window) -> Result<Option<Pid>, Error> {
            Ok(match x.get_prop(w, self.atom_pid)? {
                Some(d) => Some(Cursor::new(d).read_u32::<LittleEndian>()?),
                None => None,
            })
        }
    }
    let get_pid = GetPid { atom_pid: atom_pid };

    struct CheckPid {
        tree: bool,
        pid: Pid,
    }
    impl CheckPid {
        fn apply(&self, got: Pid) -> Result<bool, Error> {
            if self.tree {
                let expected = self.pid as i32;
                let mut at: i32 = got as i32;
                loop {
                    if at == expected {
                        return Ok(true);
                    }
                    let mut pstatus = String::new();
                    BufReader::new(File::open(
                        Path::new("/proc").join(at.to_string()).join("status"),
                    )?)
                    .read_to_string(&mut pstatus)?;
                    lazy_static! {
                        static ref RE: Regex = Regex::new("(?m)^PPid:\\s+(\\d+)$").unwrap();
                    }
                    at = RE
                        .captures(&pstatus)
                        .as_err()?
                        .get(1)
                        .as_err()?
                        .as_str()
                        .parse()?;
                    if at <= 1 {
                        return Ok(false);
                    }
                }
            } else {
                Ok(got == self.pid)
            }
        }
    }
    let check_pid = CheckPid {
        tree: tree,
        pid: pid,
    };

    info!("Scanning existing windows for pid {}", pid);
    {
        let x = x.lock().unwrap();
        for w in x.query_tree(x.root_window())? {
            match get_pid.apply(&x, w).unwrap_or_else(|e| {
                error!("Error scanning windows: {}", e);
                None
            }) {
                Some(got_pid) => {
                    trace!("Scanned window {:x?}, found pid {}", w, got_pid);
                    if check_pid.apply(got_pid)? {
                        info!("Window {:x?} matched pid!", w);
                        x.listen(w, PropertyChangeMask);
                        set_icon.apply(&x, w)?;
                    }
                }
                None => {
                    debug!("Scanned window {:x?} has no pid, watching.", w);
                    x.listen(w, PropertyChangeMask);
                }
            }
        }
    }

    struct ProcessEvent {
        x: Arc<Mutex<X11>>,
        suppress: TtlCache<xlib::Window, u32>,
        atom_icon: xlib::Atom,
        atom_pid: xlib::Atom,
        get_pid: GetPid,
        set_icon: SetIcon,
        check_pid: CheckPid,
    }

    impl ProcessEvent {
        fn set_icon2(&mut self, w: xlib::Window) -> Result<(), Error> {
            const COUNT: i32 = 5;
            const fn calc_delay(i: i32) -> Duration {
                Duration::from_millis((1 << (i as u64 * 2)) * 10)
            }
            const MAX_DURATION: Duration = calc_delay(COUNT); // longer than delay of last iteration/sum of delays

            self.suppress.insert(w, 1, MAX_DURATION);

            struct DelaySet {
                x: Arc<Mutex<X11>>,
                w: xlib::Window,
                set_icon: SetIcon,
                count: i32,
            }
            impl DelaySet {
                fn apply(mut self) -> Result<(), Error> {
                    self.count += 1;
                    info!(
                        "Setting icon for window {:x?} due to icon change or PID association! (count {}/{}, d {})",
                        self.w, self.count + 1, COUNT, calc_delay(self.count).as_millis()
                    );
                    {
                        let x = self.x.lock().unwrap();
                        self.set_icon.apply(&x, self.w)?;
                    }
                    if self.count + 1 < COUNT {
                        tokio::spawn(
                            Delay::new(Instant::now().add(calc_delay(self.count)))
                                .map_err(Error::from)
                                .and_then(move |_| self.apply())
                                .map_err(|e| error!("Error in delay set_icon: {}", e)),
                        );
                    }
                    Ok(())
                }
            }
            let delay_set = DelaySet {
                count: -1,
                x: self.x.clone(),
                w: w,
                set_icon: self.set_icon.clone(),
            };
            delay_set.apply()?;
            Ok(())
        }

        fn apply(&mut self, event: XEvent) -> Option<()> {
            match self.apply_inner(event) {
                Ok(_) => None,
                Err(e) => {
                    debug!("Error in X event loop: {}", e);
                    None
                }
            }
        }

        fn apply_inner(&mut self, event: XEvent) -> Result<(), Error> {
            let x = self.x.lock().unwrap();

            match event.get_type() {
                xlib::CreateNotify => {
                    let e: xlib::XCreateWindowEvent = event.into();
                    let w = e.window;
                    match self.get_pid.apply(&x, w)? {
                        Some(event_pid) => {
                            trace!("Scanned new window {:x?}, found pid {}", x.root_window(), w);
                            if self.check_pid.apply(event_pid)? {
                                info!("New window {:x?} matched pid!", w);
                                x.listen(w, PropertyChangeMask);
                                drop(x);
                                self.set_icon2(w)?;
                            }
                        }
                        None => {
                            debug!("Scanned new window {:x?} has no pid, watching.", w);
                            x.listen(w, PropertyChangeMask);
                        }
                    }
                }
                xlib::PropertyNotify => {
                    let e: xlib::XPropertyEvent = event.into();
                    let w = e.window;

                    // Check for interesting properties
                    if e.atom == self.atom_pid {
                        trace!("Window {:x?} pid changed.", w);
                        self.set_icon.apply(&x, w)?;
                    } else if e.atom == self.atom_icon {
                        trace!("Window {:x?} icon changed.", w);
                    } else {
                        return Ok(());
                    }

                    // Check pid relevance
                    let event_pid = match self.get_pid.apply(&x, w)? {
                        Some(event_pid) => event_pid,
                        None => {
                            trace!("Window {:x?} properties changed but still no pid.", w);
                            return Ok(());
                        }
                    };
                    if !self.check_pid.apply(event_pid)? {
                        debug!(
                            "Window {:x?} associated itself with another pid ({}), unwatching.",
                            w, event_pid
                        );
                        x.listen(w, NoEventMask);
                        return Ok(());
                    }

                    // Prevent feedback
                    if self.suppress.contains_key(&w) {
                        return Ok(());
                    }

                    // Change the icon with care, yet firmly
                    drop(x);
                    self.set_icon2(w)?;
                }
                _ => (),
            }
            Ok(())
        }
    }
    let mut process_event = ProcessEvent {
        x: x.clone(),
        suppress: TtlCache::new(100),
        atom_icon: atom_icon,
        atom_pid: atom_pid,
        get_pid: get_pid,
        set_icon: set_icon,
        check_pid: check_pid,
    };
    Ok(Box::new(
        x.stream()
            .filter_map(move |e| process_event.apply(e))
            .into_future()
            .map(|_| unreachable!())
            .map_err(|_| unreachable!()),
    ))
}

macro_rules! APP {
    () => {
        env!("CARGO_PKG_NAME")
    };
}

fn errmain() -> Result<(), Error> {
    const SUB_ATTACH: &'static str = "attach";
    const SUB_RUN: &'static str = "run";
    const ARG_ICON: &'static str = "icon";
    const ARG_PID: &'static str = "pid";
    const ARG_PROGRAM: &'static str = "program";
    const ARG_ARGUMENTS: &'static str = "arguments";
    const ARG_VERBOSE: &'static str = "verbosity";
    const ARG_TREE: &'static str = "tree";
    const ARG_V_LEVEL_TRACE: &'static str = "trace";
    const ARG_V_LEVEL_DEBUG: &'static str = "debug";
    const ARG_V_LEVEL_INFO: &'static str = "info";
    const ARG_V_LEVEL_ERROR: &'static str = "error";

    let matches = App::new(APP!())
        .setting(AppSettings::SubcommandRequired)
        .version(env!("CARGO_PKG_VERSION"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .about("Replace X11 icons for a process tree")
        .arg(
            Arg::with_name(ARG_ICON)
                .help("Icon filename")
                .takes_value(true)
                .required(true),
        )
        .arg(
            Arg::with_name(ARG_VERBOSE)
                .help("Log verbosity")
                .short("v")
                .takes_value(true)
                .default_value(ARG_V_LEVEL_ERROR)
                .possible_value(ARG_V_LEVEL_TRACE)
                .possible_value(ARG_V_LEVEL_DEBUG)
                .possible_value(ARG_V_LEVEL_INFO)
                .possible_value(ARG_V_LEVEL_ERROR),
        )
        .arg(
            Arg::with_name(ARG_TREE)
                .help("Change icons for all child processes")
                .short("t"),
        )
        .subcommand(
            SubCommand::with_name(SUB_ATTACH)
                .about("Run command and replace icon of all windows")
                .arg(
                    Arg::with_name(ARG_PID)
                        .help("PID of process tree to change the icon of")
                        .takes_value(true)
                        .required(true),
                ),
        )
        .subcommand(
            SubCommand::with_name(SUB_RUN)
                .about("Run command and replace icon of all windows")
                .arg(
                    Arg::with_name(ARG_PROGRAM)
                        .help("Program to run")
                        .takes_value(true)
                        .required(true),
                )
                .arg(
                    Arg::with_name(ARG_ARGUMENTS)
                        .help("Program arguments")
                        .multiple(true)
                        .takes_value(true),
                ),
        )
        .get_matches();

    TermLogger::init(
        match matches.value_of(ARG_VERBOSE).unwrap() {
            ARG_V_LEVEL_TRACE => LevelFilter::Trace,
            ARG_V_LEVEL_DEBUG => LevelFilter::Debug,
            ARG_V_LEVEL_INFO => LevelFilter::Info,
            ARG_V_LEVEL_ERROR => LevelFilter::Error,
            _ => unreachable!(),
        },
        Config {
            time: Some(Level::Error),
            level: Some(Level::Error),
            thread: Some(Level::Debug),
            target: Some(Level::Debug),
            location: Some(Level::Trace),
            time_format: None,
            offset: *Local::now().offset(),
            filter_allow: Some(&["replaceicon"]),
            filter_ignore: None,
        },
        TerminalMode::Mixed,
    )
    .unwrap();

    let mut runtime = runtime::current_thread::Builder::new()
        .clock(Clock::system())
        .build()?;

    let icon = matches.value_of(ARG_ICON).unwrap().into();
    let tree = matches.is_present(ARG_TREE);
    match matches.subcommand() {
        (SUB_ATTACH, Some(subargs)) => {
            runtime.spawn(attach(
                subargs.value_of(ARG_PID).unwrap().parse()?,
                icon,
                tree,
            )?);
        }
        (SUB_RUN, Some(subargs)) => {
            let child = Command::new(subargs.value_of(ARG_PROGRAM).unwrap())
                .args(subargs.values_of(ARG_ARGUMENTS).unwrap_or_default())
                .spawn_async()?;
            let attached = attach(child.id(), icon, tree)?;
            let child = child
                .map(|status| {
                    info!(
                        "Child exited with status: {}",
                        status
                            .code()
                            .map(|s| s.to_string())
                            .unwrap_or("(killed by signal)".into())
                    )
                })
                .map_err(|e| error!("Child handle failed with error: {}", e));
            runtime.spawn(
                child
                    .select(attached)
                    .map(|_v| info!("Finished!"))
                    .map_err(|_e| unreachable!()),
            );
        }
        _ => unreachable!(),
    };

    info!("Starting monitoring!");
    runtime.run()?;
    info!("Done!");

    Ok(())
}

fn main() {
    match errmain() {
        Ok(..) => {
            let _ = std::io::stdout().flush();
            let _ = std::io::stderr().flush();
        }
        Err(Error(e)) => {
            error!("Exiting due to error: {}", e);
            let _ = std::io::stdout().flush();
            let _ = std::io::stderr().flush();
            ::std::process::exit(1);
        }
    }
}
